﻿using System;

namespace OnSive.Netflix.PersonalData.Playground
{
    class Program
    {
        const string ZIP_FILE_PATH = @"C:\Insert\Your\Path\netflix-report.zip";

        static void Main(string[] args)
        {
            var netflixParser = new NetflixParser(ZIP_FILE_PATH);

            foreach (var accountDetaillItem in netflixParser.AccountDetails)
            {
                Console.WriteLine($" - {accountDetaillItem.EmailAddress}");
            }

            Console.ReadKey();
        }
    }
}
