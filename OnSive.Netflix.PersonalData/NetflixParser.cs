﻿using OnSive.Netflix.PersonalData.Classes;
using OnSive.Netflix.PersonalData.Classes.Account;
using OnSive.Netflix.PersonalData.Classes.Clickstream;
using OnSive.Netflix.PersonalData.Classes.Content_Interaction;
using OnSive.Netflix.PersonalData.Classes.Customer_Service;
using OnSive.Netflix.PersonalData.Classes.Devices;
using OnSive.Netflix.PersonalData.Classes.IpAddresses;
using OnSive.Netflix.PersonalData.Classes.Messages;
using OnSive.Netflix.PersonalData.Classes.PaymentAndBilling;
using OnSive.Netflix.PersonalData.Classes.Profiles;
using OnSive.Netflix.PersonalData.Classes.Surveys;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace OnSive.Netflix.PersonalData
{
    /// <summary>
    ///     This class contains all the personal data which can be exportet through the Netflix account settings
    /// </summary>
    public partial class NetflixParser
    {
        private readonly string sBaseDirectory;

        /// <summary>
        ///     Constructor for the personal data parser
        /// </summary>
        /// <param name="sZipOrExtractedPath">The patch to the the extracted zip file directory</param>
        /// <exception cref="PersonalDataNotFoundException">
        ///     The netflix-report.zip file or the already excracted directory could not be found at '<paramref
        ///     name="sZipOrExtractedPath" />'!
        /// </exception>
        /// <exception cref="InvalidDataException">
        ///     The archive is not a valid zip archive. -or- An archive entry was not found or was corrupt. -or- An
        ///     archive entry was compressed by using a compression method that is not supported.
        /// </exception>
        public NetflixParser(string sZipOrExtractedPath)
        {
            string sZipFilePath = sZipOrExtractedPath;
            bool bZipFileExists = File.Exists(sZipOrExtractedPath);

            if (!bZipFileExists && !Directory.Exists(sZipOrExtractedPath))
            {
                throw new PersonalDataNotFoundException($"The netflix-report.zip file could not be found at '{sZipOrExtractedPath}'!");
            }

            if (bZipFileExists)
            {
                sZipOrExtractedPath = Path.Combine(Path.GetTempPath(), $"netflix-parser.{Guid.NewGuid()}");
                Directory.CreateDirectory(sZipOrExtractedPath);
                ZipFile.ExtractToDirectory(sZipFilePath, sZipOrExtractedPath);
            }

            this.sBaseDirectory = sZipOrExtractedPath;
            LegacyParser = new NetflixLegacyParser(sZipOrExtractedPath);

            if (bZipFileExists)
            {
                this.Load();
                Directory.Delete(sZipOrExtractedPath, true);
            }
        }

        /// <summary>
        ///     Contains all legacy properties
        /// </summary>
        public NetflixLegacyParser LegacyParser { get; private set; }

        private List<AccountDetailItem> _AccountDetails { get; set; }

        private List<SubscriptionHistoryItem> _SubscriptionHistory { get; set; }

        private List<ClickstreamItem> _Clickstream { get; set; }

        private List<IndicatedPreferenceItem> _IndicatedPreferences { get; set; }

        private List<InteractiveTitleItem> _InteractiveTitles { get; set; }

        private List<MyListItem> _MyList { get; set; }

        private List<PlaybackRelatedEventItem> _PlaybackRelatedEvents { get; set; }

        private List<RatingItem> _Ratings { get; set; }

        private List<SearchHistoryItem> _SearchHistory { get; set; }

        private List<ViewingActivityItem> _ViewingActivity { get; set; }

        private List<ChatTranscriptItem> _ChatTranscripts { get; set; }

        private List<CSContactItem> _CSContact { get; set; }

        private List<DeviceItem> _Devices { get; set; }

        private List<IpAddressItem> _IpAddresses { get; set; }

        private List<MessageSentByNetflixItem> _MessagesSentByNetflix { get; set; }

        private List<BillingHistoryItem> _BillingHistory { get; set; }

        private List<GiftSubscriptionItem> _GiftSubscriptions { get; set; }

        private List<AvatarHistoryItem> _AvatarHistory { get; set; }

        private List<ProfileItem> _Profiles { get; set; }

        private List<ProductCancellationSurveyItem> _ProductCancellationSurvey { get; set; }

        /// <summary>
        ///     Loads all data into the properties
        /// </summary>
        /// <returns>If everything was loaded correctly</returns>
        public bool Load()
        {
            try
            {
                _ = AccountDetails;
                _ = SubscriptionHistory;
                _ = Clickstream;
                _ = IndicatedPreferences;
                _ = InteractiveTitles;
                _ = MyList;
                _ = PlaybackRelatedEvents;
                _ = Ratings;
                _ = SearchHistory;
                _ = ViewingActivity;
                _ = ChatTranscripts;
                _ = CSContact;
                _ = Devices;
                _ = IpAddresses;
                _ = MessagesSentByNetflix;
                _ = BillingHistory;
                _ = GiftSubscriptions;
                _ = AvatarHistory;
                _ = Profiles;
                _ = ProductCancellationSurvey;

                return LegacyParser.Load();
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        ///     Loads all data into the properties
        /// </summary>
        /// <returns>If everything was loaded correctly</returns>
        public async Task<bool> LoadAsync() => await Task.Run(() => Load());

        /// <summary>
        ///     This table will include either (a) the information that you provided to Netflix when you registered to
        ///     use our service or (b) the information as you have updated it since registration (for example, if you
        ///     update your email or added a phone number). This table aslo includes information about your current
        ///     account settings for settings such as playback and communications.
        /// </summary>
        public List<AccountDetailItem> AccountDetails
        {
            get
            {
                if (_AccountDetails == null)
                {
                    _AccountDetails = _AccountDetails.Get(sBaseDirectory);
                }
                return _AccountDetails;
            }
        }

        /// <summary>
        ///     This table contains details of the subscription(s) you have had with Netflix, such as the subscription
        ///     plan name and the time period you subscribed to that particular plan.
        /// </summary>
        public List<SubscriptionHistoryItem> SubscriptionHistory
        {
            get
            {
                if (_SubscriptionHistory == null)
                {
                    _SubscriptionHistory = _SubscriptionHistory.Get(sBaseDirectory);
                }
                return _SubscriptionHistory;
            }
        }

        /// <summary>
        ///     This table provides details of actions taken while navigating through the Netflix website while in a
        ///     logged in state.
        /// </summary>
        public List<ClickstreamItem> Clickstream
        {
            get
            {
                if (_Clickstream == null)
                {
                    _Clickstream = _Clickstream.Get(sBaseDirectory);
                }
                return _Clickstream;
            }
        }


        /// <summary>
        ///     Upon first use of a profile, profile users are invited to select TV shows and movies they watched before
        ///     they subscribed to Netflix, as well as TV shows and movies in which they have an interest ("Indicated
        ///     Preferences"). This feature is only available on our website (not on mobile devices) and may be skipped,
        ///     which is why this information may not be available. This information is used to give intial guidance to
        ///     our recommendation systems.
        /// </summary>
        public List<IndicatedPreferenceItem> IndicatedPreferences
        {
            get
            {
                if (_IndicatedPreferences == null)
                {
                    _IndicatedPreferences = _IndicatedPreferences.Get(sBaseDirectory);
                }
                return _IndicatedPreferences;
            }
        }

        /// <summary>
        ///     This table provides details of choices selected while viewing an interactive TV show or movie.
        /// </summary>
        public List<InteractiveTitleItem> InteractiveTitles
        {
            get
            {
                if (_InteractiveTitles == null)
                {
                    _InteractiveTitles = _InteractiveTitles.Get(sBaseDirectory);
                }
                return _InteractiveTitles;
            }
        }

        /// <summary>
        ///     My List is created by selecting the “+” symbol in a TV show or movie information page while you are
        ///     browsing through our catalogue.
        /// </summary>
        public List<MyListItem> MyList
        {
            get
            {
                if (_MyList == null)
                {
                    _MyList = _MyList.Get(sBaseDirectory);
                }
                return _MyList;
            }
        }

        /// <summary>
        ///     This table provides details of actions taken during a viewing session of an episode of a TV show or a
        ///     movie.
        /// </summary>
        public List<PlaybackRelatedEventItem> PlaybackRelatedEvents
        {
            get
            {
                if (_PlaybackRelatedEvents == null)
                {
                    _PlaybackRelatedEvents = _PlaybackRelatedEvents.Get(sBaseDirectory);
                }
                return _PlaybackRelatedEvents;
            }
        }

        /// <summary>
        ///     This table contains details of TV show or movie ratings. The rating might have been in the form of stars
        ///     or thumbs up/down depending on when it was made.
        /// </summary>
        public List<RatingItem> Ratings
        {
            get
            {
                if (_Ratings == null)
                {
                    _Ratings = _Ratings.Get(sBaseDirectory);
                }
                return _Ratings;
            }
        }

        /// <summary>
        ///     No description provided by Netflix.
        /// </summary>
        public List<SearchHistoryItem> SearchHistory
        {
            get
            {
                if (_SearchHistory == null)
                {
                    _SearchHistory = _SearchHistory.Get(sBaseDirectory);
                }
                return _SearchHistory;
            }
        }

        /// <summary>
        ///     No description provided by Netflix.
        /// </summary>
        public List<ViewingActivityItem> ViewingActivity
        {
            get
            {
                if (_ViewingActivity == null)
                {
                    _ViewingActivity = _ViewingActivity.Get(sBaseDirectory);
                }
                return _ViewingActivity;
            }
        }

        /// <summary>
        ///     This table provides the details of contact with Customer Service by chat transcripts (where applicable).
        ///     At the end of a chat, customers are given the option to request a copy of the transcript.  
        ///     !!ChatTranscripts is theoreticly implemented, but I need some test data! If you had some contact with
        ///     the  Netflix support it would be great if you could send the ChatTranscripts file to service@onsive.net.
        ///     You can censor private data of corse ;)!!
        /// </summary>
        public List<ChatTranscriptItem> ChatTranscripts
        {
            get
            {
                if (_ChatTranscripts == null)
                {
                    _ChatTranscripts = _ChatTranscripts.Get(sBaseDirectory);
                }
                return _ChatTranscripts;
            }
        }

        /// <summary>
        ///     This table contains the following information... You don't say Netflix   !!ChatTranscripts is
        ///     theoreticly implemented, but I need some test data! If you had some contact with the  Netflix support it
        ///     would be great if you could send the ChatTranscripts file to service@onsive.net.  You can censor private
        ///     data of corse ;)!!
        /// </summary>
        public List<CSContactItem> CSContact
        {
            get
            {
                if (_CSContact == null)
                {
                    _CSContact = _CSContact.Get(sBaseDirectory);
                }
                return _CSContact;
            }
        }

        /// <summary>
        ///     This table contains details of devices that have been linked to your Netflix member account. Please note
        ///     that blanks in this table indicate that there was no playback on a profile from the device associated
        ///     with the listed "esn" (defined below). In other words, a profile may have been accesssed on that device,
        ///     but no content was watched. Instances of Netflix acces to retrieve information while processing
        ///     inquiries (if any) related to your account may also appear on the list of devices in this table.
        /// </summary>
        public List<DeviceItem> Devices
        {
            get
            {
                if (_Devices == null)
                {
                    _Devices = _Devices.Get(sBaseDirectory);
                }
                return _Devices;
            }
        }

        /// <summary>
        ///     This table contains information associated with the last time a particular device was used to stream
        ///     from a particular IP address. The table only includes the device type, so it is possible to see the same
        ///     device type and same IP address in multiple entries. In thos cases, the streaming was from a different
        ///     device of the same type (e.g., an iPhone 5 and an iPhone 6, or two different iPhones of the same
        ///     generation).
        /// </summary>
        public List<IpAddressItem> IpAddresses
        {
            get
            {
                if (_IpAddresses == null)
                {
                    _IpAddresses = _IpAddresses.Get(sBaseDirectory);
                }
                return _IpAddresses;
            }
        }

        /// <summary>
        ///     This table contains details of messages that you received from Netflix in accordance with your
        ///     preferences , as well as the details of your interactions with links contained in messages you received
        ///     from Netflix.
        /// </summary>
        public List<MessageSentByNetflixItem> MessagesSentByNetflix
        {
            get
            {
                if (_MessagesSentByNetflix == null)
                {
                    _MessagesSentByNetflix = _MessagesSentByNetflix.Get(sBaseDirectory);
                }
                return _MessagesSentByNetflix;
            }
        }

        /// <summary>
        ///     This table contains payment details you have provided to Netflix. In addition, this table contains
        ///     information about the charges we have made or attempted to make to your method of payment for your
        ///     subscription.
        /// </summary>
        public List<BillingHistoryItem> BillingHistory
        {
            get
            {
                if (_BillingHistory == null)
                {
                    _BillingHistory = _BillingHistory.Get(sBaseDirectory);
                }
                return _BillingHistory;
            }
        }

        /// <summary>
        ///     This table contains details of Netflix gift subscriptions you have purchased or redeemed.
        /// </summary>
        public List<GiftSubscriptionItem> GiftSubscriptions
        {
            get
            {
                if (_GiftSubscriptions == null)
                {
                    _GiftSubscriptions = _GiftSubscriptions.Get(sBaseDirectory);
                }
                return _GiftSubscriptions;
            }
        }

        /// <summary>
        ///     This table contains details of avatar icons chosen for a particular profile.
        /// </summary>
        public List<AvatarHistoryItem> AvatarHistory
        {
            get
            {
                if (_AvatarHistory == null)
                {
                    _AvatarHistory = _AvatarHistory.Get(sBaseDirectory);
                }
                return _AvatarHistory;
            }
        }

        /// <summary>
        ///     This table contains details of profiles that have been created within your Netflix member accounts
        /// </summary>
        public List<ProfileItem> Profiles
        {
            get
            {
                if (_Profiles == null)
                {
                    _Profiles = _Profiles.Get(sBaseDirectory);
                }
                return _Profiles;
            }
        }

        /// <summary>
        ///     The Surveys table contains information associated with Netflix post-subscription cancellation surveeys
        ///     you have taken or otherwise interacted with.
        /// </summary>
        public List<ProductCancellationSurveyItem> ProductCancellationSurvey
        {
            get
            {
                if (_ProductCancellationSurvey == null)
                {
                    _ProductCancellationSurvey = _ProductCancellationSurvey.Get(sBaseDirectory);
                }
                return _ProductCancellationSurvey;
            }
        }
    }
}
