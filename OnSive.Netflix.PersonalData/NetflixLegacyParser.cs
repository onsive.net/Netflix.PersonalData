﻿using OnSive.Netflix.PersonalData.Classes;
using OnSive.Netflix.PersonalData.Classes.SocialMediaConnections;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace OnSive.Netflix.PersonalData
{
    /// <summary>
    ///     Legacy personal data properties
    /// </summary>
    public class NetflixLegacyParser
    {
        private readonly string sBaseDirectory;

        /// <summary>
        ///     Constructor for the legacy personal data parser
        /// </summary>
        /// <param name="sZipOrExtractedPath">The patch to the the extracted zip file directory</param>
        /// <exception cref="PersonalDataNotFoundException">
        ///     The netflix-report.zip file or the already excracted directory could not be found at '<paramref
        ///     name="sZipOrExtractedPath" />'!
        /// </exception>
        /// <exception cref="InvalidDataException">
        ///     The archive is not a valid zip archive. -or- An archive entry was not found or was corrupt. -or- An
        ///     archive entry was compressed by using a compression method that is not supported.
        /// </exception>
        public NetflixLegacyParser(string sZipOrExtractedPath)
        {
            string sZipFilePath = sZipOrExtractedPath;
            bool bZipFileExists = File.Exists(sZipOrExtractedPath);

            if (!bZipFileExists && !Directory.Exists(sZipOrExtractedPath))
            {
                throw new PersonalDataNotFoundException($"The netflix-report.zip file could not be found at '{sZipOrExtractedPath}'!");
            }

            if (bZipFileExists)
            {
                sZipOrExtractedPath = Path.Combine(Path.GetTempPath(), $"netflix-parser.{Guid.NewGuid()}");
                ZipFile.ExtractToDirectory(sZipFilePath, sZipOrExtractedPath);
            }

            this.sBaseDirectory = sZipOrExtractedPath;

            if (bZipFileExists)
            {
                this.Load();
                Directory.Delete(sZipFilePath);
            }
        }

        /// <summary>
        ///     Loads all data into the properties
        /// </summary>
        /// <returns>If everything was loaded correctly</returns>
        public bool Load()
        {
            try
            {
                _ = SocialMediaConnections;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        ///     Loads all data into the properties
        /// </summary>
        /// <returns>If everything was loaded correctly</returns>
        public async Task<bool> LoadAsync() => await Task.Run(() => Load());

        private List<SocialMediaConnectionItem> _SocialMediaConnections { get; set; }

        /// <summary>
        ///     This table includes details of the records we hold regarding the Facebook account to which you may have
        ///     connected to your Netflix member account (if applicable, please note that this is a legacy feature).
        /// </summary>
        public List<SocialMediaConnectionItem> SocialMediaConnections
        {
            get
            {
                if (_SocialMediaConnections == null)
                {
                    _SocialMediaConnections = _SocialMediaConnections.Get(sBaseDirectory);
                }
                return _SocialMediaConnections;
            }
        }
    }
}
