﻿using System;

namespace OnSive.Netflix.PersonalData.Enums
{
    /// <summary>
    ///     indicates who created the chat message.
    /// </summary>
    public enum enChatUser
    {
        /// <summary>
        ///     Customer Support
        /// </summary>
        AGENT,

        /// <summary>
        ///     Customer
        /// </summary>
        CUSTOMER
    }
}
