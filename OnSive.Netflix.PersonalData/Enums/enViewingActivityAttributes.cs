﻿using System;

namespace OnSive.Netflix.PersonalData.Enums
{
    /// <summary>
    ///     Additional details of interactions with streamed content
    /// </summary>
    [Flags]
    public enum enViewingActivityAttributes
    {
        /// <summary>
        ///     Without attributes.
        /// </summary>
        None,

        /// <summary>
        ///     means that the viewer did not interact with that TV show or movie.
        /// </summary>
        Autoplayed_UserAction_None,

        /// <summary>
        ///     means that the viewer either interacted with the TV show or movie (such as clicking the box art and
        ///     viewing the TV show or movie page while the auto-played content plays), or that the auto-played content
        ///     was watched longer than 2 minutes.
        /// </summary>
        Autoplayed_UserAction_Unspecified,

        /// <summary>
        ///     means that the viewer interacted with the TV show or movie in a browser, by clicking the video player
        ///     controls or using keyboard shortcuts.
        /// </summary>
        Autoplayed_UserAction_UserInteraction,

        /// <summary>
        ///     indicates that the TV show or movie was marked "hide from viewing history" in Account settings.
        /// </summary>
        ViewWasHidden,

        /// <summary>
        ///     indicates that the member can make choices during playback, to control what happens next.
        /// </summary>
        HasBranchedPlayback
    }
}
