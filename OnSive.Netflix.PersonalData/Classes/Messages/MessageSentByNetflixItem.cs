﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Messages
{
    /// <summary>
    ///     This table contains details of messages that you received from Netflix in accordance with your preferences ,
    ///     as well as the details of your interactions with links contained in messages you received from Netflix.
    /// </summary>
    public class MessageSentByNetflixItem
    {
        /// <summary>
        ///     the name of the profile associated with the message from Netflix.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the UTC date and time the message was sent by Netflix.
        /// </summary>
        [Name("Sent Utc Ts")]
        public string _SentUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    SentUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time the message was sent by Netflix.
        /// </summary>
        [Ignore]
        public DateTime SentUtcTs { get; set; }

        /// <summary>
        ///     the type of message sent.
        /// </summary>
        [Name("Message Name")]
        public string MessageName { get; set; }

        /// <summary>
        ///     the method by which the message was sent (e.g., email, push notification, SMS, in-app).
        /// </summary>
        [Name("Channel")]
        public string Channel { get; set; }

        /// <summary>
        ///     the country associated with the Netflix subscription at the time the message was sent.
        /// </summary>
        [Name("Country Iso Code")]
        public string CountryIsoCode { get; set; }

        /// <summary>
        ///     the country where the account was created.
        /// </summary>
        [Name("Account Locale")]
        public string AccountLocale { get; set; }

        /// <summary>
        ///     the language in which the message was sent.
        /// </summary>
        [Name("Email Locale")]
        public string EmailLocale { get; set; }

        /// <summary>
        ///     the name of the TV show or movie (if any) referenced in the message.
        /// </summary>
        [Name("Title Name")]
        public string TitleName { get; set; }

        /// <summary>
        ///     the domain of the member's email address
        /// </summary>
        [Name("Email Domain Name")]
        public string EmailDomainName { get; set; }

        /// <summary>
        ///     the URL the message receiver clicked on within the message.
        /// </summary>
        [Name("Link Url")]
        public string LinkUrl { get; set; }

        /// <summary>
        ///     the UTC date and time the Link Url was clicked on.
        /// </summary>
        [Name("Click Utc Ts")]
        public string _ClickUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ClickUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time the Link Url was clicked on.
        /// </summary>
        [Ignore]
        public DateTime ClickUtcTs { get; set; }

        /// <summary>
        ///     The device to which the message was sent.
        /// </summary>
        [Name("Device Model")]
        public string DeviceModel { get; set; }

        /// <summary>
        ///     The number of instances a message url was clicked.
        /// </summary>
        [Name("Click Cnt")]
        public int ClickCnt { get; set; }
    }
}
