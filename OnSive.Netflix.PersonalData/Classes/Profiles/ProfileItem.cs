﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Profiles
{
    /// <summary>
    ///     This table contains details of profiles that have been created within your Netflix member accounts
    /// </summary>
    public class ProfileItem
    {
        /// <summary>
        ///     the name of the profile chosen by the profile user.
        /// </summary>
        [JsonProperty("profileName")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the UTC date and time that the profile was activated.
        /// </summary>
        [JsonProperty("profileCreationtime")]
        public DateTime ProfileCreationtime { get; set; }

        /// <summary>
        ///     the maximum maturity level designating which TV shows or movies are shown to the profile user.
        /// </summary>
        [JsonProperty("maturityLevel")]
        public string MaturityLevel { get; set; }

        /// <summary>
        ///     the profile user's language preference.
        /// </summary>
        [JsonProperty("primaryLang")]
        public string PrimaryLang { get; set; }

        /// <summary>
        ///     indicates whether the next episode in a series will automatically start playing once the previous
        ///     episode has ended.
        /// </summary>
        [JsonProperty("hasAutoPlayback")]
        public string HasAutoPlayback { get; set; }

        /// <summary>
        ///     indicates wether the profile user has specified the maximum streaming quality they wish to receive. For
        ///     example, a profile user may specify a lower streaming quality to reduce bandwidth usage.
        /// </summary>
        [JsonProperty("maxStreamQuality")]
        public string MaxStreamQuality { get; set; }
    }
}
