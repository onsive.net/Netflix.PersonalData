﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Profiles
{
    /// <summary>
    ///     This table contains details of avatar icons chosen for a particular profile.
    /// </summary>
    public class AvatarHistoryItem
    {
        /// <summary>
        ///     code associated with previous icons (avatars) used, if any.
        /// </summary>
        [JsonProperty("Icons")]
        public string[] Icons { get; set; }

        /// <summary>
        ///     the name of the profile chosen by the profile user.
        /// </summary>
        [JsonProperty("profileName")]
        public string ProfileName { get; set; }
    }
}
