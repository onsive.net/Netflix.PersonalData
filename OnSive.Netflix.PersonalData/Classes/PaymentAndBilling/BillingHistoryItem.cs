﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.PaymentAndBilling
{
    /// <summary>
    ///     This table contains payment details you have provided to Netflix. In addition, this table contains
    ///     information about the charges we have made or attempted to make to your method of payment for your
    ///     subscription.
    /// </summary>
    public class BillingHistoryItem
    {
        /// <summary>
        ///     The UTC date the method of payment on file was charged.
        /// </summary>
        [Name("Transaction Date")]
        public string _TransactionDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    TransactionDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     The UTC date the method of payment on file was charged.
        /// </summary>
        [Ignore]
        public DateTime TransactionDate { get; set; }

        /// <summary>
        ///     The UTC date of the start of the subscription billing period (for example, 21 March).
        /// </summary>
        [Name("Service Period Start Date")]
        public string _ServicePeriodStartDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ServicePeriodStartDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     The UTC date of the start of the subscription billing period (for example, 21 March).
        /// </summary>
        [Ignore]
        public DateTime ServicePeriodStartDate { get; set; }

        /// <summary>
        ///     The UTC date of the end of the subscription billing period (for example, 21 March).
        /// </summary>
        [Name("Service Period End Date")]
        public string _ServicePeriodEndDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ServicePeriodEndDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     The UTC date of the end of the subscription billing period (for example, 21 March).
        /// </summary>
        [Ignore]
        public DateTime ServicePeriodEndDate { get; set; }

        /// <summary>
        ///     The type of charge incurred.
        /// </summary>
        /// <value>
        ///     For example, “subscription”, means that you were charged the amount for the subscription service;
        ///     “upgrade”means that you were charged the amount because you upgraded your subscription to a different
        ///     plan.
        /// </value>
        [Name("Description")]
        public string Description { get; set; }

        /// <summary>
        ///     The method of payment associated with your account.
        /// </summary>
        [Name("Payment Type")]
        public string PaymentType { get; set; }

        /// <summary>
        ///     The last four digits of your method of payment, if you pay (or have paid) with a debit or credit card.
        /// </summary>
        [Name("Mop Last 4")]
        public string MopLast4 { get; set; }

        /// <summary>
        ///     The UTC date your method of payment was applied to your account.
        /// </summary>
        [Name("Mop Creation Date")]
        public string _MopCreationDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    MopCreationDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     The UTC date your method of payment was applied to your account.
        /// </summary>
        [Ignore]
        public DateTime MopCreationDate { get; set; }

        /// <summary>
        ///     indicates the Payment processor used for the payment transaction.
        /// </summary>
        /// <value>
        ///     For example, “PAYMENTEC” is a specific payment processor.
        /// </value>
        [Name("Mop Pmt Processor Desc")]
        public string MopPmtProcessorDesc { get; set; }

        /// <summary>
        ///     The total cost of your subscription plan without tax included which represents your sale amount.
        /// </summary>
        [Name("Item Price Amt")]
        public double ItemPriceAmt { get; set; }

        /// <summary>
        ///     The currency in which your method of payment is charged.
        /// </summary>
        [Name("Currency")]
        public string Currency { get; set; }

        /// <summary>
        ///     The tax amount applied to your subscription service sale amount.
        /// </summary>
        [Name("Tax Amt")]
        public double TaxAmt { get; set; }

        /// <summary>
        ///     The total amount charged for the subscription including tax amount and sale amount.
        /// </summary>
        [Name("Gross Sale Amt")]
        public double GrossSalesAmt { get; set; }

        /// <summary>
        ///     indicates the specific stage that a payment transaction has entered.
        /// </summary>
        /// <value>
        ///     For example, “SALE” indicates when the subscription renewal began. “CAPTURED” indicates that payment was
        ///     captured by our payment processor.
        /// </value>
        [Name("Pmt Txn Type")]
        public string PmtTxnType { get; set; }

        /// <summary>
        ///     The status of the payment transaction.
        /// </summary>
        /// <value>
        ///     For example, “declined” means that the transaction was declined, “approved” means that the payment
        ///     transaction was approved and the method of payment was charged, and “pending” means the transaction is
        ///     pending decline or approval.
        /// </value>
        [Name("Pmt Status")]
        public string PmtStatus { get; set; }

        /// <summary>
        ///     The result of the charges we have made or attempted to make on your account for your subscription.
        /// </summary>
        /// <example>
        ///     For example, “COMPLETED” means that the charge was successful; “SETTLED” is a financial term meaning
        ///     that the fee in the captured transaction has reached Netflix, and “AUTHORIZATIONS” are requests sent by
        ///     Netflixto the financial institution to verify that the payment method provided to Netflix works. These
        ///     requests are not charges, but in some cases they may affect the available account balance. In the case
        ///     of gift cards, “APPLIED” refers to the value of the giftcard applied to your account, and “DEDUCTED”
        ///     refers to the charge deducted from the balance of the gift card.
        /// </example>
        [Name("Final Invoice Result")]
        public string FinalInvoiceResult { get; set; }

        /// <summary>
        ///     The country in which the account subscription is initiated by providing a method of payment.
        /// </summary>
        [Name("Country")]
        public string Country { get; set; }

        /// <summary>
        ///     The UTC date of the next date on which the member will be billed.
        /// </summary>
        [Name("Next Billing Date")]
        public string _NextBillingDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    NextBillingDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     The UTC date of the next date on which the member will be billed.
        /// </summary>
        [Ignore]
        public DateTime NextBillingDate { get; set; }
    }
}
