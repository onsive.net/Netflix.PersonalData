﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.PaymentAndBilling
{
    /// <summary>
    ///     This table contains details of Netflix gift subscriptions you have purchased or redeemed.
    /// </summary>
    public class GiftSubscriptionItem
    {
        /// <summary>
        ///     the gift subscription code expiration date, if applicable.
        /// </summary>
        [JsonProperty("expirationDate")]
        public DateTime? ExpirationDate { get; set; }

        /// <summary>
        ///     the description of the purchased gift subscription.
        /// </summary>
        [JsonProperty("offerDescription")]
        public string OfferDescription { get; set; }

        /// <summary>
        ///     the UTC  date and time of the gift subscription redemption.
        /// </summary>
        [JsonProperty("redemptionDate")]
        public DateTime? RedemptionDate { get; set; }

        /// <summary>
        ///     the name of the person for whom the gift subscription was purchased.
        /// </summary>
        [JsonProperty("redeemerName")]
        public string RedeemerName { get; set; }

        /// <summary>
        ///     indicates the code generated to redeem the gift.
        /// </summary>
        [JsonProperty("giftCode")]
        public string GiftCode { get; set; }

        /// <summary>
        ///     the UTC date and time of the gift subscription purchase.
        /// </summary>
        [JsonProperty("purchaseDate")]
        public DateTime PurchaseDate { get; set; }

        /// <summary>
        ///     the country (based on IP address) where the gift subscription was purchased.
        /// </summary>
        [JsonProperty("purchaseCountryCode")]
        public string PurchaseCountryCode { get; set; }

        /// <summary>
        ///     the purchase price of the gift subscription.
        /// </summary>
        [JsonProperty("purchasePrice")]
        public double PurchasePrice { get; set; }

        /// <summary>
        ///     the amount by which the purchase price was discounted, if applicable.
        /// </summary>
        [JsonProperty("purchaseDiscounted")]
        public double? PurchaseDiscounted { get; set; }

        /// <summary>
        ///     indicates the purchaser of the gift subscription.
        /// </summary>
        [JsonProperty("purchasedBy")]
        public string PurchasedBy { get; set; }

        /// <summary>
        ///     the custom message the purchaser of the gift subscription drafted to accompany the gift subscription
        ///     sent to the recipient, if applicable.
        /// </summary>
        [JsonProperty("customizationMessage")]
        public string CustomizationMessage { get; set; }
    }
}
