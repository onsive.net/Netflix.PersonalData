﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.IpAddresses
{
    /// <summary>
    ///     This table contains information associated with the last time a particular device was used to stream from a
    ///     particular IP address. The table only includes the device type, so it is possible to see the same device
    ///     type and same IP address in multiple entries. In thos cases, the streaming was from a different device of
    ///     the same type (e.g., an iPhone 5 and an iPhone 6, or two different iPhones of the same generation).
    /// </summary>
    public class IpAddressItem
    {
        /// <summary>
        ///     unique identifier of a device, which may be assigned by Netflix, the manufacturer of the device, the
        ///     manufacturer o a specific component such as a processor, or the software/firmware on the device
        /// </summary>
        [Name("esn")]
        public string ESN { get; set; }

        /// <summary>
        ///     the country /(based on IP address) from which streaming took place.
        /// </summary>
        [Name("country")]
        public string Country { get; set; }

        /// <summary>
        ///     the localized description of the devicee used for streaming; if no localized translation is available,
        ///     the English descriptiion is used
        /// </summary>
        [Name("localizedDeviceDescription")]
        public string LocalizedDeviceDescription { get; set; }

        /// <summary>
        ///     the English description of the device used for streaming.
        /// </summary>
        [Name("deviceDescription")]
        public string DeviceDescription { get; set; }

        /// <summary>
        ///     the Internet Protocol address used by the device for streaming
        /// </summary>
        [Name("ip")]
        public string IP { get; set; }

        /// <summary>
        ///     the state/province/region associated with the given IP address.
        /// </summary>
        [Name("regionCodeDisplayName")]
        public string RegionCodeDisplayName { get; set; }

        /// <summary>
        ///     the UTC date and time when the device started streaming using the given IP address.
        /// </summary>
        [Name("ts")]
        public string _TS
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    TS = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time when the device started streaming using the given IP address.
        /// </summary>
        [Ignore]
        public DateTime TS { get; set; }
    }
}
