﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Account
{
    /// <summary>
    ///     This table will include either (a) the information that you provided to Netflix when you registered to use
    ///     our service or (b) the information as you have updated it since registration (for example, if you update
    ///     your email or added a phone number). This table aslo includes information about your current account
    ///     settings for settings such as playback and communications.
    /// </summary>
    public class AccountDetailItem
    {
        /// <summary>
        ///     the account owner's fist name.
        /// </summary>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        ///     the account owner's last name.
        /// </summary>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        ///     the account owner's email address.
        /// </summary>
        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        /// <summary>
        ///     the account owner's phone number (if any).
        /// </summary>
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        ///     the country associated with the account (based on IP address) when the email address associated with the
        ///     account was first entered into our system.
        /// </summary>
        [JsonProperty("countryOfRegistration")]
        public string CountryOfRegistration { get; set; }

        /// <summary>
        ///     the country associated with the account (based on IP address)
        /// </summary>
        [JsonProperty("countryOfSignup")]
        public string CountryOfSignup { get; set; }

        /// <summary>
        ///     the account owner's preferred language.
        /// </summary>
        [JsonProperty("primaryLang")]
        public string PrimaryLang { get; set; }

        /// <summary>
        ///     indicates whether the next episode in a series will play automatically once the previous episode has
        ///     ended.
        /// </summary>
        [JsonProperty("hasAutoPlayback")]
        public bool HasAutoPlayback { get; set; }

        /// <summary>
        ///     by default this is set to 'false' and will say 'true' if you have been presented with our cookie banner.
        ///
        /// </summary>
        [JsonProperty("cookieDisclosure")]
        public bool CookieDisclosure { get; set; }

        /// <summary>
        ///     indicates wether or not the subscription is currently in the free trial period.
        /// </summary>
        [JsonProperty("inFreeTrial")]
        public bool InFreeTrial { get; set; }

        /// <summary>
        ///     the membership status at the time this Response was requested.
        /// </summary>
        [JsonProperty("membershipStatus")]
        public string MembershipStatus { get; set; }

        /// <summary>
        ///     the UTC date and time when the member most recently activated their Netflix subscription.
        /// </summary>
        [JsonProperty("customerCreationTimeStamp")]
        public DateTime CustomerCreationTimeStamp { get; set; }

        /// <summary>
        ///     indicates wether as member has rejoined the service after cancelling the subscription but before account
        ///     deletion.
        /// </summary>
        [JsonProperty("hasRejoined")]
        public bool HasRejoined { get; set; }

        /// <summary>
        ///     indicates wether a member is eligible to recive "Now on Netflix" email notifications (emails about newly
        ///     added movies, TV shows and seasons, plus personalized suggestions and allerts).
        /// </summary>
        [JsonProperty("emailConsentNetflixNews")]
        public string EmailConsentNetflixNews { get; set; }

        /// <summary>
        ///     indicates wether a member is eligible to recive "Netflix Udpate" (emails about new and enhanced features
        ///     and tips for getting the most out of the Netflix service).
        /// </summary>
        [JsonProperty("emailConsentMemberNews")]
        public string EmailConsentMemberNews { get; set; }

        /// <summary>
        ///     indicates whether a member is eligible to receive “Netflix Offers” (emails about special offers and
        ///     promotions).
        /// </summary>
        [JsonProperty("emailConsentNetflixOffersViaEmail")]
        public string EmailConsentNetflixOffersViaEmail { get; set; }

        /// <summary>
        ///     indicates whether a member is eligible to receive “Netflix Surveys” (in which we ask questions about how
        ///     to make Netflix a better product for you).
        /// </summary>
        [JsonProperty("emailConsentNetflixSurveys")]
        public string EmailConsentNetflixSurveys { get; set; }

        /// <summary>
        ///     indicates whether a member is eligible to receive transactional SMS messages.
        /// </summary>
        [JsonProperty("smsConsentTransactional")]
        public string SmsConsentTransactional { get; set; }

        /// <summary>
        ///     indicates whether a member is eligible to receive SMS messages related to the Netflix service like New
        ///     Season alerts.
        /// </summary>
        [JsonProperty("smsConsentInformational")]
        public string SmsConsentInformational { get; set; }

        /// <summary>
        ///     refers to “Test Participation” in the Settings area in the “Account” page settings, which allows members
        ///     to participate in tests to help improve the Netflix experience and see potential changes before they are
        ///     available to all members
        /// </summary>
        [JsonProperty("testParticipation")]
        public string TestParticipation { get; set; }

        /// <summary>
        ///     indicates whether the account owner is eligible to receive Netflix messages through the WhatsApp
        ///     messaging service.
        /// </summary>
        [JsonProperty("whatsAppConsent")]
        public string WhatsAppConsent { get; set; }

        /// <summary>
        ///     indicates whether you are eligible to receive Netflix promotional communications on third party
        ///     services.
        /// </summary>
        [JsonProperty("marketingCommunicationsMatchedIdentifiers")]
        public string MarketingCommunicationsMatchedIdentifiers { get; set; }
    }
}
