﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Account
{
    /// <summary>
    ///     This table contains details of the subscription(s) you have had with Netflix, such as the subscription plan
    ///     name and the time period you subscribed to that particular plan.
    /// </summary>
    public class SubscriptionHistoryItem
    {
        /// <summary>
        ///     the UTC date and time when the subscription was created.
        /// </summary>
        [Name("Subscription Opened Ts")]
        public string _SubscriptionOpenedTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    SubscriptionOpenedTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time when the subscription was created.
        /// </summary>
        [Ignore]
        public DateTime SubscriptionOpenedTs { get; set; }

        /// <summary>
        ///     indicates whether the account owner received a free trial when signing up for the subscription.
        /// </summary>
        [Name("Is Free Trial At Signup")]
        public string IsFreeTrialAtSignup { get; set; }

        /// <summary>
        ///     the UTC date and time when the subscription was cancelled (if applicable).
        /// </summary>
        [Name("Subscription Closed Ts")]
        public string _SubscriptionClosedTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    SubscriptionClosedTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time when the subscription was cancelled (if applicable).
        /// </summary>
        [Ignore]
        public DateTime? SubscriptionClosedTs { get; set; }

        /// <summary>
        ///     indicates whether the subscription was cancelled voluntarily by the member or automatically cancelled by
        ///     Netflix.
        /// </summary>
        [Name("Is Customer Initiated Cancel")]
        public string IsCustomerInitiatedCancel { get; set; }

        /// <summary>
        ///     indicates the type of subscription at the time of sign up.
        /// </summary>
        [Name("Signup Plan Category")]
        public string SignupPlanCategory { get; set; }

        /// <summary>
        ///     indicates the number of DVDs out at a time or concurrent streams allowed (as applicable).
        /// </summary>
        [Name("Signup Max Concurrent Streams")]
        public int SignupMaxConcurrentStreams { get; set; }

        /// <summary>
        ///     indicates the streaming video quality at the time of sign up.
        /// </summary>
        [Name("Signup Max Streaming Quality")]
        public string SignupMaxStreamingQuality { get; set; }

        /// <summary>
        ///     the UTC date and timeof a plan change (if any).
        /// </summary>
        [Name("Plan Change Date")]
        public string _PlanChangeDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    PlanChangeDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and timeof a plan change (if any).
        /// </summary>
        [Ignore]
        public DateTime? PlanChangeDate { get; set; }

        /// <summary>
        ///     indicates the previous subscription plan type (if any).
        /// </summary>
        [Name("Plan Change Old Category")]
        public string PlanChangeOldCategory { get; set; }

        /// <summary>
        ///     indicates the maximum number of concurrent streams available on the prior plan (if applicable).
        /// </summary>
        [Name("Plan Change Old Max Concurrent Streams")]
        public int PlanChangeOldMaxConcurrentStreams { get; set; }

        /// <summary>
        ///     indicates the maximum streaming quality on the prior plan (if applicable).
        /// </summary>
        [Name("Plan Change Old Max Streaming Quality")]
        public string PlanChangeOldMaxStreamingQuality { get; set; }

        /// <summary>
        ///     indicates the current plan type for the subscription, if a plan change was made
        /// </summary>
        [Name("Plan Change New Category")]
        public string PlanChangeNewCategory { get; set; }

        /// <summary>
        ///     inidactes the maximum concurrent streams availible on the current plan, if a plan change was made.
        /// </summary>
        [Name("Plan Change New Max Concurrent Streams")]
        public int PlanChangeNewMaxConcurrentStreams { get; set; }

        /// <summary>
        ///     indicates the maximum streaming quality of the current plan, if a plan change was made.
        /// </summary>
        [Name("Plan Change New Max Streaming Quality")]
        public string PlanChangeNewMaxStreamQuality { get; set; }

        /// <summary>
        ///     indicates the reason for cancellation (if any).
        /// </summary>
        [Name("Cancellation Reason")]
        public string CancellationReason { get; set; }
    }
}
