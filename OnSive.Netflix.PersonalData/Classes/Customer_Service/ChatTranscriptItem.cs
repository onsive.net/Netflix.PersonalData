﻿using CsvHelper.Configuration.Attributes;
using OnSive.Netflix.PersonalData.Enums;
using System;
using System.IO;

namespace OnSive.Netflix.PersonalData.Classes.Customer_Service
{
    /// <summary>
    ///     This table provides the details of contact with Customer Service by chat transcripts (where applicable). At
    ///     the end of a chat, customers are given the option to request a copy of the transcript.
    /// </summary>
    public class ChatTranscriptItem
    {
        /// <summary>
        ///     the UTC date and time of the beginning of the chat with our Customer Service agent.
        /// </summary>
        [Name("Contact Start Utc Ts")]
        public string _ContactStartUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ContactStartUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time of the beginning of the chat with our Customer Service agent.
        /// </summary>
        [Ignore]
        public DateTime ContactStartUtcTs { get; set; }

        /// <summary>
        ///     indicates who created the chat message.
        /// </summary>
        [Name("User Type")]
        public string _UserType
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (Enum.TryParse<enChatUser>(value, out enChatUser ViewingActivityAttributes))
                    {
                        UserType = ViewingActivityAttributes;
                    }
                    else
                    {
                        throw new InvalidDataException($"The passed value '{value}' is not valid as a 'UserType|ChatUser'!");
                    }
                }
            }
        }

        /// <summary>
        ///     indicates who created the chat message.
        /// </summary>
        [Ignore]
        public enChatUser UserType { get; set; }


        /// <summary>
        ///     the text of the messages exchanged between Agent and Customer.
        /// </summary>
        [Name("Customer Message")]
        public string CustomerMessage { get; set; }

        /// <summary>
        ///     the UTC date of the chat.
        /// </summary>
        [Name("Fact Utc Date")]
        public string _FactUtcDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    FactUtcDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date of the chat.
        /// </summary>
        [Ignore]
        public DateTime FactUtcDate { get; set; }

        /// <summary>
        ///     indicates how the chat ended, as detected by the Netflix system. For example, “agent closed” means the
        ///     Agent ended the chat.
        /// </summary>
        [Name("End State")]
        public string EndState { get; set; }

        /// <summary>
        ///     indicates the number of responses provided in the optional survey at the end of the chat (if any).
        /// </summary>
        [Name("Survey Response Count")]
        public string SurveyResponseCount { get; set; }

        /// <summary>
        ///     indicates whether Customer took the optional survey at the end of the chat and whether Customer
        ///     indicated Customer was dissatisfied with the customer service experience.
        /// </summary>
        [Name("Negative Survey Response Cnt")]
        public string NegativeSurveyResponseCnt { get; set; }

        /// <summary>
        ///     indicates whether Customer took the optional survey at the end of the chat or call and whether Customer
        ///     indicated Customer was dissatisfied with the customer service experience, (if any).
        /// </summary>
        [Name("Cancel Reason")]
        public string CancelReason { get; set; }

        /// <summary>
        ///     the reason for subscription cancellation the Customer provided to the Agent, if not a predetermined
        ///     “Cancel Reason” category (if any).
        /// </summary>
        [Name("Cancel Reason Explained")]
        public string CancelReasonExplained { get; set; }
    }
}
