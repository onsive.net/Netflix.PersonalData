﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Customer_Service
{
    /// <summary>
    ///     This table contains the following information... You don't say Netflix
    /// </summary>
    public class CSContactItem
    {
        /// <summary>
        ///     the UTC date and time of the beginning of the chat or call with our Customer Service agent.
        /// </summary>
        [Name("Contact Start Utc Ts")]
        public string _ContactStartUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ContactStartUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time of the beginning of the chat or call with our Customer Service agent.
        /// </summary>
        [Ignore]
        public DateTime ContactStartUtcTs { get; set; }


        /// <summary>
        ///     the UTC date and time of the end of the chat or call with our Customer Service agent.
        /// </summary>
        [Name("Contact End Utc Ts")]
        public string _ContactEndUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    ContactEndUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time of the end of the chat or call with our Customer Service agent.
        /// </summary>
        [Ignore]
        public DateTime ContactEndUtcTs { get; set; }

        /// <summary>
        ///     indicates whether the Customer took the optional survey at the end of the chat or call and whether
        ///     Customer indicated Customer was dissatisfied with the customer service experience.
        /// </summary>
        [Name("Negative Survey Response Count")]
        public int NegativeSurveyResponseCount { get; set; }

        /// <summary>
        ///     indicates the number of responses provided in the optional survey at the end of the chat or call (if
        ///     any).
        /// </summary>
        [Name("Survey Response Count")]
        public int SurveyResponseCount { get; set; }

        /// <summary>
        ///     indicates the language in which the chat or call took place.
        /// </summary>
        [Name("Language")]
        public string Language { get; set; }

        /// <summary>
        ///     indicates how the chat or call ended, as detected by the Netflix system. For example, “agent closed”
        ///     means the Agent ended the chat.
        /// </summary>
        [Name("End State")]
        public string EndState { get; set; }

        /// <summary>
        ///     indicates whether Customer took the optional survey at the end of the chat or call and whether Customer
        ///     indicated Customer was dissatisfied with the customer service experience, (if any).
        /// </summary>
        [Name("Cancel Reason")]
        public string CancelReason { get; set; }

        /// <summary>
        ///     the reason for subscription cancellation the Customer provided to the Agent, if not a predetermined
        ///     “Cancel Reason” category (if any).
        /// </summary>
        [Name("Cancel Reason Explained")]
        public string CancelReasonExplained { get; set; }

        /// <summary>
        ///     The UTC date of the chat.
        /// </summary>
        [Name("Fact Utc Date")]
        public string FactUtcDate { get; set; }
    }
}
