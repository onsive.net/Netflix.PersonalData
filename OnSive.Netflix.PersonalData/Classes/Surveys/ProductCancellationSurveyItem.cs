﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Surveys
{
    /// <summary>
    ///     The Surveys table contains information associated with Netflix post-subscription cancellation surveeys you
    ///     have taken or otherwise interacted with.
    /// </summary>
    public class ProductCancellationSurveyItem
    {
        /// <summary>
        ///     indicates the subject matter of the survey.
        /// </summary>
        [JsonProperty("Question Name")]
        public string QuestionName { get; set; }

        /// <summary>
        ///     indicates the question posed during the survey.
        /// </summary>
        [JsonProperty("Question Current Txt")]
        public string QuestionCurrentTxt { get; set; }

        /// <summary>
        ///     indicates the answer category provided.
        /// </summary>
        [JsonProperty("Answer Name")]
        public string AnswerName { get; set; }

        /// <summary>
        ///     indicates the provided response to the question asked.
        /// </summary>
        [JsonProperty("Answer Current Text")]
        public string AnswerCurrentText { get; set; }

        /// <summary>
        ///     the text (if any) provided in an open text field.
        /// </summary>
        [JsonProperty("Answer Response Text")]
        public string AnswerResponseText { get; set; }

        /// <summary>
        ///     the language preference setting in which the survey was provided.
        /// </summary>
        [JsonProperty("Locale Code")]
        public string LocaleCode { get; set; }

        /// <summary>
        ///     The UTC date when the survey was taken.
        /// </summary>
        [JsonProperty("Event Utc Date")]
        public DateTime EventUtcDate { get; set; }
    }
}
