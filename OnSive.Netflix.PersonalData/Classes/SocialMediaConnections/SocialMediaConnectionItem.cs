﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.SocialMediaConnections
{
    /// <summary>
    ///     This table includes details of the records we hold regarding the Facebook account to which you may have
    ///     connected to your Netflix member account (if applicable, please note that this is a legacy feature).
    /// </summary>
    public class SocialMediaConnectionItem
    {
        /// <summary>
        ///     the social media platform to which your Netflix account is connected. "partnerIds" is the member's user
        ///     ID.
        /// </summary>
        [JsonProperty("FACEBOOK_SSO")]
        public string FacebookSSO { get; set; }

        /// <summary>
        ///     the profile name where the social media platform profile pricture is used and the member's partnerIds.
        /// </summary>
        [JsonProperty("FACEBOOK_AVATAR")]
        public string FacebookAvatar { get; set; }
    }
}
