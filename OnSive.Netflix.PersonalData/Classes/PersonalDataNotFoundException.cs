﻿using System;
using System.IO;
using System.Linq;

namespace OnSive.Netflix.PersonalData.Classes
{
    /// <summary>
    ///     Exception which is thrown on missing personal data zip file or extracted directory
    /// </summary>
    public sealed class PersonalDataNotFoundException : IOException
    {
        internal PersonalDataNotFoundException()
            : base()
        {
        }

        internal PersonalDataNotFoundException(string message)
            : base(message)
        {
        }

        internal PersonalDataNotFoundException(string message, int hresult)
            : base(message, hresult)
        {
        }

        internal PersonalDataNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
