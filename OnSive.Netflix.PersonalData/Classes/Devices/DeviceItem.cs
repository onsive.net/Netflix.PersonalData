﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Devices
{
    /// <summary>
    ///     This table contains details of devices that have been linked to your Netflix member account. Please note
    ///     that blanks in this table indicate that there was no playback on a profile from the device associated with
    ///     the listed "esn" (defined below). In other words, a profile may have been accesssed on that device, but no
    ///     content was watched. Instances of Netflix acces to retrieve information while processing inquiries (if any)
    ///     related to your account may also appear on the list of devices in this table.
    /// </summary>
    public class DeviceItem
    {
        /// <summary>
        ///     the name of the profile associated with devices that interacted with the Netflix streaming service.
        /// </summary>
        [Name("profileName")]
        public string profileName { get; set; }

        /// <summary>
        ///     unique identifier of a device, which may be assigned by Netflix, the manufacturer of the device, the
        ///     manufacturer of a specific component such as a processor, or the software/firmware on the device.
        /// </summary>
        [Name("esn")]
        public string esn { get; set; }

        /// <summary>
        ///     The device that was used to interact with the Netflix streaming service.
        /// </summary>
        [Name("deviceType")]
        public string deviceType { get; set; }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the account, either by clicking play or continuing
        ///     to stream after auto-play on a specific device, regardless of wether or not the user interacted with
        ///     playback after auto-play started.
        /// </summary>
        [Name("acctFirstPlaybackDate")]
        public string _acctFirstPlaybackDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    acctFirstPlaybackDate = dtTmp;
                }
                else
                {
                    acctFirstPlaybackDate = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the account, either by clicking play or continuing
        ///     to stream after auto-play on a specific device, regardless of wether or not the user interacted with
        ///     playback after auto-play started.
        /// </summary>
        [Ignore]
        public DateTime? acctFirstPlaybackDate { get; set; }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the account, either by clicking play or continuing
        ///     to stream after auto-play on a specific device, regardless of whether or not the user interacted with
        ///     playback after auto-play started.
        /// </summary>
        [Name("acctLastPlaybackDate")]
        public string _acctLastPlaybackDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    acctLastPlaybackDate = dtTmp;
                }
                else
                {
                    acctLastPlaybackDate = null;
                }
            }
        }


        /// <summary>
        ///     the UTC date and time playback was last initiated on the account, either by clicking play or continuing
        ///     to stream after auto-play on a specific device, regardless of whether or not the user interacted with
        ///     playback after auto-play started.
        /// </summary>
        [Ignore]
        public DateTime? acctLastPlaybackDate { get; set; }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the account, eitherby clicking play or performing
        ///     a user action after auto-play on a specific device.
        /// </summary>
        [Name("acctFirstPlaybackDateForUserGeneratedPlays")]
        public string _acctFirstPlaybackDateForUserGeneratedPlays
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    acctFirstPlaybackDateForUserGeneratedPlays = dtTmp;
                }
                else
                {
                    acctFirstPlaybackDateForUserGeneratedPlays = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the account, eitherby clicking play or performing
        ///     a user action after auto-play on a specific device.
        /// </summary>
        [Ignore]
        public DateTime? acctFirstPlaybackDateForUserGeneratedPlays { get; set; }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the account, eitherby clicking play or performing a
        ///     user action after auto-play on a specific device.
        /// </summary>
        [Name("acctLastPlaybackDateForUserGeneratedPlays")]
        public string _acctLastPlaybackDateForUserGeneratedPlays
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    acctLastPlaybackDateForUserGeneratedPlays = dtTmp;
                }
                else
                {
                    acctLastPlaybackDateForUserGeneratedPlays = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the account, eitherby clicking play or performing a
        ///     user action after auto-play on a specific device.
        /// </summary>
        [Ignore]
        public DateTime? acctLastPlaybackDateForUserGeneratedPlays { get; set; }

        /// <summary>
        ///     the first date a device was used to stream on the profile within the last two (2) years.
        /// </summary>
        [Name("profileFirstPlaybackDate")]
        public string _profileFirstPlaybackDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    profileFirstPlaybackDate = dtTmp;
                }
                else
                {
                    profileFirstPlaybackDate = null;
                }
            }
        }

        /// <summary>
        ///     the first date a device was used to stream on the profile within the last two (2) years.
        /// </summary>
        [Ignore]
        public DateTime? profileFirstPlaybackDate { get; set; }

        /// <summary>
        ///     the last date a device was sued to stream on the profile.
        /// </summary>
        [Name("profileLastPlaybackDate")]
        public string _profileLastPlaybackDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    profileLastPlaybackDate = dtTmp;
                }
                else
                {
                    profileLastPlaybackDate = null;
                }
            }
        }

        /// <summary>
        ///     the last date a device was sued to stream on the profile.
        /// </summary>
        [Ignore]
        public DateTime? profileLastPlaybackDate { get; set; }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the profile, either by clicking play or continuing
        ///     to stream after auto.pay on a specific device.
        /// </summary>
        [Name("profileFirstPlaybackDateForUserGeneratedPlays")]
        public string _profileFirstPlaybackDateForUserGeneratedPlays
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    profileFirstPlaybackDateForUserGeneratedPlays = dtTmp;
                }
                else
                {
                    profileFirstPlaybackDateForUserGeneratedPlays = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was first initiated on the profile, either by clicking play or continuing
        ///     to stream after auto.pay on a specific device.
        /// </summary>
        [Ignore]
        public DateTime? profileFirstPlaybackDateForUserGeneratedPlays { get; set; }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the profile, either by clicking play or continuing
        ///     to stream after auto.pay on a specific device.
        /// </summary>
        [Name("profileLastPlaybackDateForUserGeneratedPlays")]
        public string _profileLastPlaybackDateForUserGeneratedPlays
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    profileLastPlaybackDateForUserGeneratedPlays = dtTmp;
                }
                else
                {
                    profileLastPlaybackDateForUserGeneratedPlays = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the profile, either by clicking play or continuing
        ///     to stream after auto.pay on a specific device.
        /// </summary>
        [Ignore]
        public DateTime? profileLastPlaybackDateForUserGeneratedPlays { get; set; }

        /// <summary>
        ///     the UTC date(s) the devices were signed out of the account either by the member through the Account page
        ///     or by Customer Service
        /// </summary>
        [Name("deactivationTime")]
        public string _deactivationTime
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    deactivationTime = dtTmp;
                }
                else
                {
                    deactivationTime = null;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time playback was last initiated on the profile, either by clicking play or continuing
        ///     to stream after auto.pay on a specific device.
        /// </summary>
        [Ignore]
        public DateTime? deactivationTime { get; set; }
    }
}
