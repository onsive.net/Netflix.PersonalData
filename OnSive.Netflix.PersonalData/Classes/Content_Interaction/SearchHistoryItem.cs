﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     No description provided by Netflix.
    /// </summary>
    public class SearchHistoryItem
    {
        /// <summary>
        ///     the name of the profile from which the search request originated.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the country(based on IP address) from which the search request originated.
        /// </summary>
        [Name("Country Iso Code")]
        public string CountryIsoCode { get; set; }

        /// <summary>
        ///     the device type from which the account was accessed.
        /// </summary>
        [Name("Device")]
        public string Device { get; set; }

        /// <summary>
        ///     indication whether the search occurred on a Kids profile.
        /// </summary>
        /// <value>
        ///     In this column, the following values have the following meaning: ○ “0” means the search did not occur on
        ///     a Kids profile. ○ “1” means the search occurred on a Kids profile.
        /// </value>
        [Name("Is Kids")]
        public int _IsKids
        {
            set { IsKids = value == 1; }
        }

        /// <summary>
        ///     indication whether the search occurred on a Kids profile.
        /// </summary>
        [Ignore]
        public bool IsKids { get; set; }

        /// <summary>
        ///     the query entered in the search field.
        /// </summary>
        [Name("Query Typed")]
        public string QueryTyped { get; set; }

        /// <summary>
        ///     the TV show or movie resulting from the query typed.
        /// </summary>
        [Name("Displayed Name")]
        public string DisplayedName { get; set; }

        /// <summary>
        ///     shows actions in response to searches (such as whether a synopsis was viewed or a “play” button for a
        ///     show was clicked, or that a searched TV show or movie was added to “My List” (My List).
        /// </summary>
        [Name("Action")]
        public string Action { get; set; }

        /// <summary>
        ///     this column shows where on the search results page the Action occurred. For example, that the play
        ///     button in the show synopsis window was clicked, or that information about one of the Netflix suggested
        ///     results was viewed, etc.
        /// </summary>
        [Name("Section")]
        public string Section { get; set; }

        /// <summary>
        ///     the UTC date and time on which the search request was made.
        /// </summary>
        [Name("Utc Timestamp")]
        public string _UtcTimestamp
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    UtcTimestamp = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time on which the search request was made.
        /// </summary>
        [Ignore]
        public DateTime UtcTimestamp { get; set; }
    }
}
