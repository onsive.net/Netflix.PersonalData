﻿using CsvHelper.Configuration.Attributes;
using OnSive.Netflix.PersonalData.Enums;
using System;
using System.IO;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     No description provided by Netflix.
    /// </summary>
    public class ViewingActivityItem
    {
        /// <summary>
        ///     the name of the profile in which viewing occurred.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the UTC date and time viewing started.
        /// </summary>
        [Name("Start Time")]
        public string _StartTime
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    StartTime = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time viewing started.
        /// </summary>
        [Ignore]
        public DateTime StartTime { get; set; }

        /// <summary>
        ///     the length of the viewing session.
        /// </summary>
        [Name("Duration")]
        public string Duration { get; set; }

        /// <summary>
        ///     this column shows additional details of interactions with streamed content.
        /// </summary>
        [Name("Attributes")]
        public string _Attributes
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] sAttributes = value.Split(';');

                    for (int i = 0; i < sAttributes.Length; i++)
                    {
                        var sAttribute = sAttributes[i].Replace("_", string.Empty)
                                                       .Replace(": ", "_")
                                                       .Replace(" ", string.Empty)
                                                       .Trim();

                        if (string.IsNullOrEmpty(sAttribute))
                        {
                            continue;
                        }

                        if (Enum.TryParse(sAttribute, true, out enViewingActivityAttributes ViewingActivityAttributes))
                        {
                            if (i == 0)
                            {
                                Attributes = ViewingActivityAttributes;
                            }
                            else
                            {
                                Attributes |= ViewingActivityAttributes;
                            }
                        }
                        else
                        {
                            throw new InvalidDataException($"The passed value '{value}' is not valid as a 'ViewingActivityAttributes'!");
                        }
                    }
                }
                else
                {
                    Attributes = enViewingActivityAttributes.None;
                }
            }
        }

        /// <summary>
        ///     this column shows additional details of interactions with streamed content.
        /// </summary>
        [Ignore]
        public enViewingActivityAttributes Attributes { get; set; }

        /// <summary>
        ///     the TV show or movie viewed.
        /// </summary>
        [Name("Title")]
        public string Title { get; set; }

        /// <summary>
        ///     videos other than a TV show or movie, such as trailers or montages.
        /// </summary>
        /// <value>The reference "N/A" means not applicabel.</value>
        [Name("Supplemental Video Type")]
        public string SupplementalVideoType { get; set; }

        /// <summary>
        ///     the device type from which the TV show or movie was streamed.
        /// </summary>
        [Name("Device Type")]
        public string DeviceType { get; set; }

        /// <summary>
        ///     the most recent viewing position (relative to the total length of the TV show or movie) from the
        ///     particular playback session of the TV show or movie.
        /// </summary>
        [Name("Bookmark")]
        public string Bookmark { get; set; }

        /// <summary>
        ///     indicates whether the Bookmark is the most recent viewing position (relative to the total length of the
        ///     TV show or movie) from the most recent playback session of a TV show or movie.
        /// </summary>
        /// <value>
        ///     "Not latest view" - indicates that a particular plyback session is not the most recent plaback for the
        ///     TV show or movie and therefore the Bookmark is not the most recent.
        /// </value>
        [Name("Latest Bookmark")]
        public string LatestBookmark { get; set; }

        /// <summary>
        ///     the country from which the TV show or movie was viewed. As of 1 April 2018, for members in the European
        ///     Union traveling for up to 60 days from one member state to another member state, this is the country
        ///     where the account was created rather than the member state where the TV show or movie was viewed.
        /// </summary>
        [Name("Country")]
        public string Country { get; set; }
    }
}
