﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     My List is created by selecting the “+” symbol in a TV show or movie information page while you are browsing
    ///     through our catalogue.
    /// </summary>
    public class MyListItem
    {
        /// <summary>
        ///     the name of the profile in which the TV show or movie was added to the profile’s My List.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the name of the TV show or movie added the profile’s My List.
        /// </summary>
        [Name("Title Name")]
        public string TitleName { get; set; }

        /// <summary>
        ///     the country where the TV show or movie was added to the profile’s My List.
        /// </summary>
        [Name("Country")]
        public string Country { get; set; }

        /// <summary>
        ///     the UTC date the TV show or movie was added to the profile’s My List.
        /// </summary>
        [Name("Utc Title Add Date")]
        public string _UtcTitleAddDate
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    UtcTitleAddDate = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date the TV show or movie was added to the profile’s My List.
        /// </summary>
        [Ignore]
        public DateTime UtcTitleAddDate { get; set; }
    }
}
