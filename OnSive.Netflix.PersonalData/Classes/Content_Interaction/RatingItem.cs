﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     This table contains details of TV show or movie ratings. The rating might have been in the form of stars or
    ///     thumbs up/down depending on when it was made.
    /// </summary>
    public class RatingItem
    {
        /// <summary>
        ///     the name of the profile from which the rating originated.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the name of the TV show or movie for which the rating was given.
        /// </summary>
        [Name("Title Name")]
        public string TitleName { get; set; }

        /// <summary>
        ///     the type of rating given to a TV show or movie.
        /// </summary>
        [Name("Rating Type")]
        public string RatingType { get; set; }

        /// <summary>
        ///     the most recent star rating the profile user has given to a particular TV show or movie. In this column,
        ///     the following values have the following meaning:  ○ “0” means the profile user has deleted the rating ○
        ///     “-1” means the profile user is not interested in the TV show or movie ○ “-2” means the profile user has
        ///     indicated in Indicated Preferences (explained in the Indicated Preferences section below) that the TV
        ///     show or movie has not been seen.
        /// </summary>
        [Name("Star Value")]
        public string StarValue { get; set; }

        /// <summary>
        ///     the numeric representation of the rating given to a TV show or movie.
        /// </summary>
        /// <value>
        ///     For a thumb Rating Type, the values have the following meaning: ○ “0” means “not rated” ○ “1” means
        ///     “thumbs down” ○ “2” means “thumbs up”
        /// </value>
        [Name("Thumbs Value")]
        public string ThumbsValue { get; set; }

        /// <summary>
        ///     the device model used when rating was given.
        /// </summary>
        [Name("Device Model")]
        public string DeviceModel { get; set; }

        /// <summary>
        ///     the UTC date and time when the rating was given.
        /// </summary>
        [Name("Event Utc Ts")]
        public string _EventUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    EventUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time when the rating was given.
        /// </summary>
        [Ignore]
        public DateTime EventUtcTs { get; set; }
    }
}
