﻿using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     indicates a event that took place during the playback.
    /// </summary>
    public class PlaytraceItem
    {
        /// <summary>
        ///     indicates the type of action taken.
        /// </summary>
        /// <value>
        ///     ■ start     - start of a viewing session.■pause -pause the viewing session. ■ reposition- click and hold
        ///     the slider to move forward or backwards. ■ skip      - skip by using the +10 or -10 seconds control. ■
        ///     play      - play for the first time or resuming play after a pause. ■ transition- transition, applies to
        ///     interactive titles where you make a choice and also where you are watching a series and transition from
        ///     episode 1 right into episode 2 (for example). ■ stopped   - stopped, the end of a viewing session.
        /// </value>
        [JsonProperty("eventType")]
        public string EventType { get; set; }

        /// <summary>
        ///     indicates when the event occurred relative to the beginning of the viewing session that day.
        /// </summary>
        [JsonProperty("sessionOffsetMs")]
        public int? SessionOffsetMs { get; set; }

        /// <summary>
        ///     indicates when the event occurred relative to the beginning of the TV show or movie.
        /// </summary>
        [JsonProperty("mediaOffsetMs")]
        public int? MediaOffsetMs { get; set; }
    }
}
