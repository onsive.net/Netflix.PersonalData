﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     Upon first use of a profile, profile users are invited to select TV shows and movies they watched before
    ///     they subscribed to Netflix, as well as TV shows and movies in which they have an interest ("Indicated
    ///     Preferences"). This feature is only available on our website (not on mobile devices) and may be skipped,
    ///     which is why this information may not be available. This information is used to give intial guidance to our
    ///     recommendation systems.
    /// </summary>
    public class IndicatedPreferenceItem
    {
        /// <summary>
        ///     the name of the profile for which the preference was indicated.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the name of the TV show or movie selected from among a variable list.
        /// </summary>
        [Name("Show")]
        public string Show { get; set; }

        /// <summary>
        ///     reflects a response indicating that the selected TV show or movie has been watched before.
        /// </summary>
        [Name("Has Watched")]
        public string HasWatched { get; set; }

        /// <summary>
        ///     reflects whether the profile user expressed an interest in the TV show or movie.
        /// </summary>
        [Name("Is Interested")]
        public string IsInterested { get; set; }

        /// <summary>
        ///     the date the preferences were indicated.
        /// </summary>
        [Name("Event Date")]
        public DateTime EventDate { get; set; }
    }
}
