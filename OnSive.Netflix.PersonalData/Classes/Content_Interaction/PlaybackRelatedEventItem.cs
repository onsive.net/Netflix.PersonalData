﻿using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     This table provides details of actions taken during a viewing session of an episode of a TV show or a movie.
    ///
    /// </summary>
    public class PlaybackRelatedEventItem
    {
        /// <summary>
        ///     the name of the profile in which the TV show or movie was viewed.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the TV show or movie viewed.
        /// </summary>
        [Name("Title Description")]
        public string TitleDescription { get; set; }

        /// <summary>
        ///     the device type from which the TV show or movie was streamed.
        /// </summary>
        [Name("Device")]
        public string Device { get; set; }

        /// <summary>
        ///     the UTC date and time the viewing started.
        /// </summary>
        [Name("Playback Start Utc Ts")]
        public string _PlaybackStartUtcTs
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    PlaybackStartUtcTs = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time the viewing started.
        /// </summary>
        [Ignore]
        public DateTime PlaybackStartUtcTs { get; set; }

        /// <summary>
        ///     country
        /// </summary>
        [Name("Country")]
        public string Country { get; set; }

        /// <summary>
        ///     indicates the events that took place during the playback.
        /// </summary>
        [Name("Playtraces")]
        public string _Playtraces
        {
            set { Playtraces = JsonConvert.DeserializeObject<PlaytraceItem[]>(value); }
        }

        /// <summary>
        ///     indicates the events that took place during the playback.
        /// </summary>
        [Ignore]
        public PlaytraceItem[] Playtraces { get; set; }
    }
}
