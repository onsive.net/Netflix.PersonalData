﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Content_Interaction
{
    /// <summary>
    ///     This table provides details of choices selected while viewing an interactive TV show or movie.
    /// </summary>
    public class InteractiveTitleItem
    {
        /// <summary>
        ///     the name of the profile from which the interactive TV show or movie was viewed
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the name of the interactive TV show or movie viewed.
        /// </summary>
        [Name("Title Desc")]
        public string TitleDesc { get; set; }

        /// <summary>
        ///     the choices made during the playback.
        /// </summary>
        /// <value>
        ///     "default" - refers to the default playback selection, and is selected when an active choice is not made
        ///     and the “ok” button is not clicked during the allotted time.  “userinput” - refers to the choice made
        ///     during playback by the profile user indicated in “Choice SegmentId”.
        /// </value>
        [Name("Selection Type")]
        public string SelectionType { get; set; }

        /// <summary>
        ///     the choice indicated in the “Selection Type.”
        /// </summary>
        [Name("Choice Segment Id")]
        public string ChoiceSegmentId { get; set; }

        /// <summary>
        ///     reflects whether this path (choice) has been taken before (TRUE) or has not been taken before (FALSE).
        /// </summary>
        [Name("Has Watched")]
        public bool HasWatched { get; set; }

        /// <summary>
        ///     the device platform on which the TV show or movie was streamed.
        /// </summary>
        [Name("Source")]
        public string Source { get; set; }

        /// <summary>
        ///     the UTC date and time the choice in “Selection Type” was selected.
        /// </summary>
        [Name("Utc Timestamp")]
        public string _SourceUtcTimestamp
        {
            set
            {
                if (DateTime.TryParse(value, out DateTime dtTmp))
                {
                    SourceUtcTimestamp = dtTmp;
                }
            }
        }

        /// <summary>
        ///     the UTC date and time the choice in “Selection Type” was selected.
        /// </summary>
        [Ignore]
        public DateTime SourceUtcTimestamp { get; set; }
    }
}
