﻿using CsvHelper.Configuration.Attributes;
using System;

namespace OnSive.Netflix.PersonalData.Classes.Clickstream
{
    /// <summary>
    ///     This table provides details of actions taken while navigating through the Netflix website while in a logged
    ///     in state.
    /// </summary>
    public class ClickstreamItem
    {
        /// <summary>
        ///     the name of the profile associated with the clickstream information.
        /// </summary>
        [Name("Profile Name")]
        public string ProfileName { get; set; }

        /// <summary>
        ///     the device type from which the visit occurred.
        /// </summary>
        [Name("Source")]
        public string Source { get; set; }

        /// <summary>
        ///     the page of the Netflix service that was visited.
        /// </summary>
        [Name("Navigation Level")]
        public string NavigationLevel { get; set; }

        /// <summary>
        ///     the URL of the website from which you came to visit the Netflix site.
        /// </summary>
        [Name("Referrer Url")]
        public string ReferrerUrl { get; set; }

        /// <summary>
        ///     the URL of the Netflix website page of the visit (only available where the device type ("Source") is a
        ///     browser).
        /// </summary>
        [Name("Webpage Url")]
        public string WebpageUrl { get; set; }

        /// <summary>
        ///     the UTC date and time when the Netflix page was visited.
        /// </summary>
        [Name("Click Utc Ts")]
        public DateTime ClickUtcTs { get; set; }
    }
}
