﻿using CsvHelper;
using Newtonsoft.Json;
using OnSive.Netflix.PersonalData.Classes.Account;
using OnSive.Netflix.PersonalData.Classes.Clickstream;
using OnSive.Netflix.PersonalData.Classes.Content_Interaction;
using OnSive.Netflix.PersonalData.Classes.Customer_Service;
using OnSive.Netflix.PersonalData.Classes.Devices;
using OnSive.Netflix.PersonalData.Classes.IpAddresses;
using OnSive.Netflix.PersonalData.Classes.Messages;
using OnSive.Netflix.PersonalData.Classes.PaymentAndBilling;
using OnSive.Netflix.PersonalData.Classes.Profiles;
using OnSive.Netflix.PersonalData.Classes.SocialMediaConnections;
using OnSive.Netflix.PersonalData.Classes.Surveys;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnSive.Netflix.PersonalData.Classes
{
    internal static class Functions
    {
        internal static string Get_SubPath<T>()
        {
            var Type_Class = typeof(T);
            if (Type_Class == typeof(AccountDetailItem))
            {
                return @"Account\AccountDetails.txt";
            }
            else if (Type_Class == typeof(SubscriptionHistoryItem))
            {
                return @"Account\SubscriptionHistory.csv";
            }
            else if (Type_Class == typeof(ClickstreamItem))
            {
                return @"Clickstream\Clickstream.csv";
            }
            else if (Type_Class == typeof(IndicatedPreferenceItem))
            {
                return @"Content_Interaction\IndicatedPreferences.csv";
            }
            else if (Type_Class == typeof(InteractiveTitleItem))
            {
                return @"Content_Interaction\InteractiveTitles.csv";
            }
            else if (Type_Class == typeof(MyListItem))
            {
                return @"Content_Interaction\MyList.csv";
            }
            else if (Type_Class == typeof(PlaybackRelatedEventItem))
            {
                return @"Content_Interaction\PlaybackRelatedEvents.csv";
            }
            else if (Type_Class == typeof(RatingItem))
            {
                return @"Content_Interaction\Ratings.csv";
            }
            else if (Type_Class == typeof(SearchHistoryItem))
            {
                return @"Content_Interaction\SearchHistory.csv";
            }
            else if (Type_Class == typeof(ViewingActivityItem))
            {
                return @"Content_Interaction\ViewingActivity.csv";
            }
            else if (Type_Class == typeof(ChatTranscriptItem))
            {
                return @"Customer_Service\ChatTranscripts.txt";
            }
            else if (Type_Class == typeof(CSContactItem))
            {
                return @"Customer_Service\CSContact.txt";
            }
            else if (Type_Class == typeof(DeviceItem))
            {
                return @"Devices\Devices.csv";
            }
            else if (Type_Class == typeof(IpAddressItem))
            {
                return @"Ip_Addresses\IpAddresses.csv";
            }
            else if (Type_Class == typeof(MessageSentByNetflixItem))
            {
                return @"Messages\MessagesSentByNetflix.csv";
            }
            else if (Type_Class == typeof(BillingHistoryItem))
            {
                return @"Payment_And_Billing\BillingHistory.csv";
            }
            else if (Type_Class == typeof(GiftSubscriptionItem))
            {
                return @"Payment_And_Billing\GiftSubscriptions.txt";
            }
            else if (Type_Class == typeof(AvatarHistoryItem))
            {
                return @"Profiles\AvatarHistory.txt";
            }
            else if (Type_Class == typeof(ProfileItem))
            {
                return @"Profiles\Profiles.txt";
            }
            else if (Type_Class == typeof(SocialMediaConnectionItem))
            {
                return @"Social_Media_Connections\SocialMediaConnections.txt";
            }
            else if (Type_Class == typeof(ProductCancellationSurveyItem))
            {
                return @"Surveys\ProductCancellationSurvey.txt";
            }
            else
            {
                return null;
            }
        }

        internal static List<T> Get<T>(this List<T> List, string sBaseDirectory)
            => Parse<T>(sBaseDirectory);

        internal static async Task<List<T>> GetAsync<T>(this List<T> List, string sBaseDirectory)
            => await Task.Run(() => Parse<T>(sBaseDirectory)).ConfigureAwait(false);

        internal static List<T> Parse<T>(string sBaseDirectory)
        {
            string sPath = Path.Combine(sBaseDirectory, Get_SubPath<T>());

            if (!File.Exists(sPath))
            {
                throw new FileNotFoundException($"The file '{sPath}' could not be found!");
            }

            var sExtension = sPath.Substring(sPath.LastIndexOf('.')).ToLower();

            switch (sExtension)
            {
                case ".csv":

                    using (var oStreamReader = new StreamReader(sPath))
                    {
                        using (var oCsvReader = new CsvReader(oStreamReader, CultureInfo.InvariantCulture))
                        {
                            if (oStreamReader.ReadLine() == "We found no information for this table.")
                            {
                                return new List<T>();
                            }

                            oStreamReader.DiscardBufferedData();
                            oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin);

                            return oCsvReader.GetRecords<T>().ToList();
                        }
                    }

                case ".txt":

                    var sRows = File.ReadAllLines(sPath);
                    var a = new List<T>();

                    if (sRows[0] == "We found no information for this table.")
                    {
                        return a;
                    }

                    for (int i = 0; i < sRows.Length; i++)
                    {
                        if (string.IsNullOrWhiteSpace(sRows[i]))
                        {
                            continue;
                        }

                        var b = JsonConvert.DeserializeObject<T>(sRows[i]);
                        a.Add(b);
                    }

                    return a;

                default:
                    break;
            }

            return null;
        }

        internal static string[] SpecialSplit(string sText, char cSplit)
        {
            List<string> List_Params = new List<string>();
            int iLastIndex = 0;
            bool bIgnore = false;

            for (int i = 0; i < sText.Length; i++)
            {
                if (sText[i] == '"')
                {
                    bIgnore = !bIgnore;
                }
                else if ((sText[i] == cSplit) && !bIgnore)
                {
                    List_Params.Add(sText.Substring(iLastIndex, i - iLastIndex));
                    iLastIndex = i + 1;
                }
            }

            List_Params.Add(sText.Substring(iLastIndex, sText.Length - iLastIndex));
            return List_Params.ToArray();
        }
    }
}
