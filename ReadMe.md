
[![NuGet Badge Version](https://img.shields.io/nuget/v/OnSive.Netflix.PersonalData?style=for-the-badge)
![NuGet Badge Downloads](https://img.shields.io/nuget/dt/OnSive.Netflix.PersonalData?style=for-the-badge)](https://www.nuget.org/packages/OnSive.Netflix.PersonalData)

<br />
<div align="center">
  <img src="https://gitlab.com/onsive.net/Netflix.PersonalData/-/raw/master/OnSive.Netflix.PersonalData/Icon.png" alt="Logo" width="80" height="80">

  <h3 align="center">OnSive | Netflix PersonalData Parser</h3>

  <p align="center">
    Small libary to parse all data from a <a href="https://netflix.com/" title="Prosymbols">Netflix</a> personal data export.
  <br />
  <a href="https://gitlab.com/onsive.net/Netflix.PersonalData/-/issues/new">Report Bug</a>
    ·
  <a href="https://gitlab.com/onsive.net/Netflix.PersonalData/-/issues/new">Request Feature</a>
    ·
  <a href="https://gitlab.com/onsive.net/Netflix.PersonalData/-/tree/master/.doc">Documentation</a>
    ·
  <a href="https://gitlab.com/onsive.net/Netflix.PersonalData/-/blob/master/ChangeLog.md">Changelog</a>
  </p>
</div>

---

### Get

- Package Manager:
	```
	Install-Package OnSive.Netflix.PersonalData -Version 2.0.0
	```
- .NET CLI:
	```
	dotnet add package OnSive.Netflix.PersonalData --version 2.0.0
	```
- Package Reference:
	```
	<PackageReference Include="OnSive.Netflix.PersonalData" Version="2.0.0" />
	```
- Paket CLI:
	```
	paket add OnSive.Netflix.PersonalData --version 2.0.0
	```

---

### Usage

1. Instantiate a new `NetflixParser` object and pass the path to the exported folder (`...\netflix-report`).

	1.1 With the `netflix-report.zip` file
	```
	var netflixParser = new NetflixParser("C:\Users\My_User\Downloads\netflix-report.zip");
	```
	> **Note:** 
	> With this method, the complete dataset will be pre loaded on initialization as the zip file is unpacked and directly deleted afterwards!

	1.2 With the extracted `netflix-report.zip` directory
	```
	var netflixParser = new NetflixParser("C:\Users\My_User\Downloads\netflix-report");
	```

2. Optionally preload all data
	```
	netflixParser.Load();
	// or
	await netflixParser.LoadAsync();
	```

	> **Note:**
	> The data is loaded and cached on access.
	> With the `.Load();` method, you can preload all data. This will take a few seconds and could be done with a background worker.

3. After that you're done and can access all informations via the different lists.<br />
  Most properties got the same name like described in the `Cover sheet.pdf` which is delivered within the `netflix-report` folder.

---

### Example Code
Example for printing out all profile names and their creation time:
```
var netflixParser = new NetflixParser("C:\Users\My_User\Downloads\netflix-report");

foreach (var profileItem in netflixParser.Profiles)
{
    Console.WriteLine($"User {profileItem.ProfileName} was created at {profileItem.ProfileCreationtime}.");
}
```

---

### Q&A
Why is there such a long loading time on some properties??
> Netflix was kind enough to provide the data in a wonderful way...
> The csv files are fine, but all data in the .txt files are one Json object per line.
> This means that I have to deserialize every line in a .txt file one by one.

---

### Third Party

- CsvHelper v21.0.0 [Apache-2.0] https://joshclose.github.io/CsvHelper/
- Newtonsoft.Json v12.0.3 [MIT] https://www.newtonsoft.com/json
- Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
