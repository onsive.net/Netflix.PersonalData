# OnSive.Netflix.PersonalData
## [2.0.0] Cleanup - 2021-01-19
### Added
- Documentation under [/.dock](https://gitlab.com/onsive.net/Netflix.PersonalData/-/tree/master/.dock) (currently unavailible on GitLab pages but could be comming soon)
- Added icon in small

### Changed
- Namespace for `OnSive.Netflix.PersonalData.Classes.Payment_And_Billing` to `OnSive.Netflix.PersonalData.Classes.PaymentAndBilling`
- Namespace for `OnSive.Netflix.PersonalData.Classes.Social_Media_Connections` to `OnSive.Netflix.PersonalData.Classes.SocialMediaConnections`

### Removed
- Removed all obsolete methods from v1.0.0+