var class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser =
[
    [ "NetflixParser", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ad84e95e9e2da0407b17aa46b8b10547f", null ],
    [ "Load", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ac93969250f0b3bbb709388fc5b818206", null ],
    [ "LoadAsync", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a3d639aea2679ca8dcb2c06c995c9ca6e", null ],
    [ "_AccountDetails", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a9a91c609ab30d40b8c2cef198f0e6972", null ],
    [ "_AvatarHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a6c7334418e61299c7998d4206bbe8b13", null ],
    [ "_BillingHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a988b9bf1a7edb7772c613a2f353e02f5", null ],
    [ "_ChatTranscripts", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ad0d6d869f1d177ff5a074df44d258e26", null ],
    [ "_Clickstream", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a25b9007f87f7793f39d1e0a510c6ce2c", null ],
    [ "_CSContact", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a015ad89e6af82b8ce7d63b6ab77b0d37", null ],
    [ "_Devices", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a7786082a35ed2c0167250162e9ee8d61", null ],
    [ "_GiftSubscriptions", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a4117cccae6349d1577d525f04354e4dc", null ],
    [ "_IndicatedPreferences", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#aad0ab6f8455bd9d2d4eb1d0d1ef1187f", null ],
    [ "_InteractiveTitles", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a584e18b8d570b42261763f8cac2c631b", null ],
    [ "_IpAddresses", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a80f2315388041d75d6481af5616c1f46", null ],
    [ "_MessagesSentByNetflix", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ab662b836d8bdbf6d4c615a5b508d1f59", null ],
    [ "_MyList", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#af37d82e6afd9d4e4de310efbc702809b", null ],
    [ "_PlaybackRelatedEvents", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a26cd724993bf7d7ba41a4c5d7689e854", null ],
    [ "_ProductCancellationSurvey", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ad73dd37886528b47296b9948b26b6557", null ],
    [ "_Profiles", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a4f19841b66463e3316e2ebcf415c6a26", null ],
    [ "_Ratings", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ad2d4c5d9405cad336c3242d7d2b5e3b1", null ],
    [ "_SearchHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a9ce049605b71e765b11d7be6d6315731", null ],
    [ "_SubscriptionHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a04fe96769dbddd01714ac02a5ed377a6", null ],
    [ "_ViewingActivity", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a3dd49e868c13bfd43bdac80d35ca28ed", null ],
    [ "AccountDetails", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a5b9773bd4566e8fc68634be079b234fe", null ],
    [ "AvatarHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ac605da4596dc241bd59783f2e3a5641c", null ],
    [ "BillingHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ac42a9855461603a25530a230f6e6ecdd", null ],
    [ "ChatTranscripts", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a46242727ebfeb67a16e8031bd3e6ecce", null ],
    [ "Clickstream", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a0528a29c8a148188f64884d90bcca7cf", null ],
    [ "CSContact", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#aa22bfe6a7f03c1f238687d685249a3d3", null ],
    [ "Devices", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a9b646a1f95862b069ef920094734a700", null ],
    [ "GiftSubscriptions", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a6350a217ec3a54ed35eca5f8f63e1e1a", null ],
    [ "IndicatedPreferences", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a3a0387d6b70e347bf4bef831d662703a", null ],
    [ "InteractiveTitles", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#aed364dde6bae81526153b9ccced587e7", null ],
    [ "IpAddresses", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a874f27e84f6d013c5dd499157be09650", null ],
    [ "LegacyParser", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a6daf91b794268b12c4970144f74e2c9b", null ],
    [ "MessagesSentByNetflix", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a2eb4a8dfd45ef27d4dddccd7221a4e94", null ],
    [ "MyList", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a9ea43c1ffda00c2a50ee2b0968d5c2cd", null ],
    [ "PlaybackRelatedEvents", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#aa0b7d86b0220a6c67709adc5fe69c6e9", null ],
    [ "ProductCancellationSurvey", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a896b28768186a5576aa60d8f5817c25d", null ],
    [ "Profiles", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a05a67d00ee619f8a8c28b57e7d4cb98d", null ],
    [ "Ratings", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a9cf035834a916d9a001770f61751d868", null ],
    [ "SearchHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#aca3d9322eaf74cdcf7ed3b341405ce41", null ],
    [ "SubscriptionHistory", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#a011dadba7dc719fd3c3591847a3177e5", null ],
    [ "ViewingActivity", "class_on_sive_1_1_netflix_1_1_personal_data_1_1_netflix_parser.html#ab1f2ca867794baad5bc9460aa562567b", null ]
];